package com.app.sample.shop.model;

import java.io.Serializable;

/**
 * Created by shobo on 5/31/17.
 */

public class Categories implements Serializable {

    //Data Variables
    private String name;
    private String description;
    private String image_url;
    private String status;
    private int parent;
    private String created_by;
    private String updated_by;
    private String created_at;
    private String updated_at;


    public Categories() {

    }

    public Categories(String name,String description,String image_url, String status,int parent,String created_by,String updated_by,String created_at,String updated_at) {
        this.name = name;
        this.description = description;
        this.image_url = image_url;
        this.status = status;
        this.parent = parent;
        this.created_by = created_by;
        this.updated_by = updated_by;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    //Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        if(description.length()>100){
            return description.substring(0, 80)+"...";
        }
        return description+"...";
    }

    public String getImageUrl() {
        return image_url;
    }

    public void setImageUrl(String image_url) {
        this.image_url = image_url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }

    public String getCreatedBy() {
        return created_by;
    }

    public void setCreatedBy(String created_by) {
        this.created_by = created_by;
    }

    public String getUpdatedBy() {
        return updated_by;
    }

    public void setUpdatedBy(String updated_by) {
        this.updated_by = updated_by;
    }
    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }
    public String getUpdatedAt() {
        return updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }
}
