package com.app.sample.shop;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.view.AnimatedExpandableListView;
import com.app.sample.shop.view.AnimatedExpandableListView.AnimatedExpandableListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * This is an example usage of the AnimatedExpandableListView class.
 *
 * It is an activity that holds a listview which is populated with 100 groups
 * where each group has from 1 to 100 children (so the first group will have one
 * child, the second will have two children and so on...).
 */
public class ExpandableListViewActivity extends ActionBarActivity {

	private AnimatedExpandableListView listView;
	private ExampleAdapter adapter;private Toolbar mToolbar;
	private ActionBar actionBar;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_expandable_list_view);

		initToolbar();

        // Populate our list with groups and it's children
        List<GroupItem> items = new ArrayList<GroupItem>();
		String[] myProdsArray = getResources().getStringArray(R.array.testProductsArray);
        //new item
        GroupItem itemN = new GroupItem();
        String str = "New Items";
        itemN.title = str;
        ChildItem childN = new ChildItem();
        childN.hint = str;
        childN.category = str;
        childN.title = str;
        //new item
        itemN.items.add(childN);
        items.add(itemN);

        for(int i = 0; i< myProdsArray.length; i++){
            GroupItem item = new GroupItem();
            item.title = myProdsArray[i];
            //other categories
            ChildItem childA = new ChildItem();
            ChildItem childM = new ChildItem();
            ChildItem childF = new ChildItem();
            childA.hint = "All";
            childA.category = myProdsArray[i];
            childA.title = childA.hint+" "+ myProdsArray[i];
            childM.hint = "Male";
            childM.category = myProdsArray[i];
            childM.title = childM.hint+" "+ myProdsArray[i];
            childF.hint = "Female";
            childF.category = myProdsArray[i];
            childF.title = childF.hint+" "+ myProdsArray[i];
            item.items.add(childA);
            item.items.add(childM);
            item.items.add(childF);
            items.add(item);
        }


		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		adapter = new ExampleAdapter(this);
		adapter.setData(items);

		listView = (AnimatedExpandableListView) findViewById(R.id.list_view);
		listView.setAdapter(adapter);

		// In order to show animations, we need to use a custom click handler
		// for our ExpandableListView.
		listView.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
				// We call collapseGroupWithAnimation(int) and
				// expandGroupWithAnimation(int) to animate group
				// expansion/collapse.
				if (listView.isGroupExpanded(groupPosition)) {
					listView.collapseGroupWithAnimation(groupPosition);
				} else {
					listView.expandGroupWithAnimation(groupPosition);
				}
				return true;
			}

		});

		// Set indicator (arrow) to the right
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		//Log.v("width", width + "");
		Resources r = getResources();
		int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				50, r.getDisplayMetrics());
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			listView.setIndicatorBounds(width - px, width);
		} else {
			listView.setIndicatorBoundsRelative(width - px, width);
		}
	}

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("All Categories");

    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private static class GroupItem {
		String title;
		List<ChildItem> items = new ArrayList<ChildItem>();
	}

	private static class ChildItem {
		String title;
		String hint;
        String category;
	}

	private static class ChildHolder {
		TextView title;
        LinearLayout lyt_child;
		//TextView hint;
	}

	private static class GroupHolder {
		TextView title;
        TextView titleImage;
        ImageView textImage1;
	}

	/**
	 * Adapter for our list of {@link GroupItem}s.
	 */
	private class ExampleAdapter extends AnimatedExpandableListAdapter {
		private LayoutInflater inflater;

		private List<GroupItem> items;

		public ExampleAdapter(Context context) {
			inflater = LayoutInflater.from(context);
		}

		public void setData(List<GroupItem> items) {
			this.items = items;
		}

		@Override
		public ChildItem getChild(int groupPosition, int childPosition) {
			return items.get(groupPosition).items.get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getRealChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			ChildHolder holder;
			final ChildItem item = getChild(groupPosition, childPosition);
			if (convertView == null) {
				holder = new ChildHolder();
				convertView = inflater.inflate(R.layout.categories_child_list_item, parent,
						false);
				holder.title = (TextView) convertView
						.findViewById(R.id.textTitle);

                holder.lyt_child = (LinearLayout) convertView.findViewById(R.id.lyt_child);
				/*holder.hint = (TextView) convertView
						.findViewById(R.id.textHint);*/
				convertView.setTag(holder);
			} else {
				holder = (ChildHolder) convertView.getTag();
			}

			holder.title.setText(item.title);
			//holder.hint.setText(item.hint);

            //when you click the linear layout
            holder.lyt_child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent mIntent=new Intent(ExpandableListViewActivity.this,ActivityMain.class);
                    mIntent.putExtra("fragment", 30);
                    mIntent.putExtra("hint",item.hint);
                    mIntent.putExtra("category",item.category);
                    startActivity(mIntent);


                    Intent i = new Intent(ExpandableListViewActivity.this,ActivityMain.class);
//                    Bundle b = new Bundle();
//                    b.putString("fragments", "launch_prod_details");
//                    i.putExtra("hint",item.hint);
//                    i.putExtra("category",item.category);
//                    startActivity(i);

                    Bundle b = new Bundle();
                    b.putString("fragments", "launch_prod_details");
                    b.putString("hint",item.hint);
                    b.putString("category",item.category);
                    i.putExtra("launch_fragments", b);
                    startActivity(i);

                }
            });

			return convertView;
		}

		@Override
		public int getRealChildrenCount(int groupPosition) {
			return items.get(groupPosition).items.size();
		}

		@Override
		public GroupItem getGroup(int groupPosition) {
			return items.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return items.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			GroupHolder holder;
			GroupItem item = getGroup(groupPosition);
			if (convertView == null) {
				holder = new GroupHolder();
				convertView = inflater.inflate(R.layout.group_item, parent,
						false);
				holder.title = (TextView) convertView
						.findViewById(R.id.textTitle);
                holder.titleImage = (TextView) convertView
                        .findViewById(R.id.textImage);

                holder.textImage1 = (ImageView) convertView.findViewById(R.id.textImage1);
				convertView.setTag(holder);
			} else {
				holder = (GroupHolder) convertView.getTag();
			}

			holder.title.setText(item.title);
            holder.titleImage.setText(getResources().getString(R.string.material_icon_cart));
			holder.textImage1.setVisibility(View.GONE);
            //holder.textImage1.setImageResource(R.drawable.ic_nav_accessories);

			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}

	}

}
