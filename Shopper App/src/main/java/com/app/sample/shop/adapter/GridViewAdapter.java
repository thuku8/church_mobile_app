package com.app.sample.shop.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.QuickOptionsGridItem;
import com.app.sample.shop.widget.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridViewAdapter extends ArrayAdapter<QuickOptionsGridItem> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<QuickOptionsGridItem> data = new ArrayList<QuickOptionsGridItem>();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList<QuickOptionsGridItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageTitle = (TextView) row.findViewById(R.id.text);
            holder.image = (ImageView) row.findViewById(R.id.image);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }


        QuickOptionsGridItem item = data.get(position);
        holder.imageTitle.setText(item.getTitle());
        holder.image.setImageBitmap(item.getImage());
        Picasso.with(context).load(R.drawable.ic_sales_medium).resize(100, 100).transform(new CircleTransform()).into(holder.image);
        //Picasso.with(context).load(R.drawable.drawableName).into(imageView);
        return row;
    }

    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }
}