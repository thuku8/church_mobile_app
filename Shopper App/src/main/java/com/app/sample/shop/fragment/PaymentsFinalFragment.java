package com.app.sample.shop.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.activities.Orders.OrderDetailsActivity;
import com.app.sample.shop.activities.Payments.ActivityCheckOut;
import com.app.sample.shop.model.Orders;

/**
 * Created by shobo on 6/6/17.
 */

public class PaymentsFinalFragment extends Fragment {

    private static final String ARG_POSITION = "position";

    private Orders model;
    private LinearLayout layout;
    private TextView icon;
    private Button bt_checkstatus;

    public PaymentsFinalFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        model = (Orders) getArguments().getSerializable(ActivityCheckOut.TAG_ORDER_MODEL);


        ((ActivityCheckOut) getActivity()).setActionBarTitle(ActivityCheckOut.PAYMENT_FINAL_STRING);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_payments_final,container, false);
        layout = (LinearLayout) rootView.findViewById(R.id.lyt_parent);

        icon = (TextView) rootView.findViewById(R.id.fragment_payments_final_icon);

        layout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        layout.invalidate();
        icon.setText(R.string.material_icon_check_full);


        bt_checkstatus = (Button) rootView.findViewById(R.id.bt_checkstatus);
        bt_checkstatus.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                OrderDetailsActivity.navigate((ActivityCheckOut)getActivity(), rootView.findViewById(R.id.lyt_parent), model);
            }
        });

        ViewCompat.setElevation(rootView, 50);
        return rootView;
    }
}
