package com.app.sample.shop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.OrderProduct;
import com.app.sample.shop.widget.CircleTransform;
import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shobo on 6/8/17.
 */

public class OrderProductsAdapter extends RecyclerView.Adapter<OrderProductsAdapter.ViewHolder> {

    private final int mBackground;
    private List<OrderProduct> original_items = new ArrayList<>();

    private final TypedValue mTypedValue = new TypedValue();

    private Context ctx;

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView prodTitle;
        public TextView prodQtyAndCost;
        public TextView prodCost;
        public TextView total;
        public ImageView imageView;

        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            prodTitle = (TextView) v.findViewById(R.id.prodTitle);
            prodQtyAndCost = (TextView) v.findViewById(R.id.prodQtyAndCost);
            prodCost = (TextView) v.findViewById(R.id.prodCost);
            imageView = (ImageView) v.findViewById(R.id.imageView);
        }
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public OrderProductsAdapter(Context ctx, List<OrderProduct> items) {
        this.ctx = ctx;
        original_items = items;
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
    }

    @Override
    public OrderProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_products, parent, false);
        //v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        OrderProductsAdapter.ViewHolder vh = new OrderProductsAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final OrderProductsAdapter.ViewHolder holder, final int position) {
        final OrderProduct p = original_items.get(position);
        holder.prodTitle.setText(String.valueOf(p.getName()));
        holder.prodQtyAndCost.setText(String.valueOf(p.getQuantity())+ " * "+String.valueOf(p.getPrice()));
        holder.prodCost.setText("$"+String.valueOf(((int) p.getPrice())* (p.getQuantity())));

        Picasso.with(ctx).load(p.getImg()).transform(new CircleTransform()).into(holder.imageView);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return original_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
