package com.app.sample.shop.activities.Payments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.app.sample.shop.ActivityMain;
import com.app.sample.shop.R;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.fragment.AddressSelectionFragment;
import com.app.sample.shop.fragment.PaymentTypesFragment;
import com.app.sample.shop.fragment.PaymentsFinalFragment;

/**
 * Created by shobo on 6/6/17.
 */

public class ActivityCheckOut extends AppCompatActivity {

    public static String TAG_LONG = "com.app.sample.shop.tagDeliveryLong";
    public static String TAG_LAT = "com.app.sample.shop.tagDeliveryLat";
    public static String TAG_PLACE_DISTANCE = "com.app.sample.shop.tagDeliveryDistance";
    public static String TAG_PLACE_NAME = "com.app.sample.shop.tagDeliveryPlaceName";
    public static String TAG_PAY_TYPE = "com.app.sample.shop.tagPayType";
    public static String TAG_PAY_TYPE_id = "com.app.sample.shop.tagPayTypeId";
    public static String TAG_ORDER_MODEL = "com.app.sample.shop.tagOrderModel";


    private Toolbar mToolbar;
    private ActionBar actionBar;
    private Menu menu;
    private View parent_view;
    private GlobalVariable global;

    //selecting the fragment
    private int pageNumber = 0;
    public final static int PAYMENTS_TYPES_FRAGMENT = 0;
    public final static int PAYMENTS_ADDRESS_FRAGMENT = 1;
    public final static int PAYMENTS_COUPON_FRAGMENT = 2;
    public final static int PAYMENTS_FINAL_FRAGMENT = 3;

    public final static String PAYMENT_TYPES_STRING = "PAYMENT METHODS";
    public final static String PAYMENT_ADDRESS_STRING = "SELECT ADRRESS";
    public final static String PAYMENT_COUPON_STRING = "COUPONS";
    public final static String PAYMENT_FINAL_STRING = "CHECK OUT";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        parent_view = findViewById(R.id.main_content);
        global = (GlobalVariable) getApplication();

        initToolbar();

        changeFragment(PAYMENTS_ADDRESS_FRAGMENT,PAYMENT_ADDRESS_STRING);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
    }



    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                Intent home = new Intent(ActivityCheckOut.this, ActivityMain.class);
                startActivity(home);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void changeFragment(int fragmentType,String title) {
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        //setActionBarTitle(title);
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (fragmentType) {
            case PAYMENTS_TYPES_FRAGMENT:
                fragment = new PaymentTypesFragment();
                break;
            case PAYMENTS_ADDRESS_FRAGMENT:
                fragment = new AddressSelectionFragment();
                break;
            case PAYMENTS_FINAL_FRAGMENT:
                fragment = new PaymentsFinalFragment();
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment,title);
            fragmentTransaction.commit();
            //initToolbar();
        }
    }

    public void setActionBarTitle(String title) {
        //changing action bar title
        if (title.equals("")) {
            actionBar.setTitle(getString(R.string.app_name));
        }else{
            actionBar.setTitle(title);
        }
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    //remember to change this
    @Override
    public void onBackPressed() {
        doExitApp();
    }

}
