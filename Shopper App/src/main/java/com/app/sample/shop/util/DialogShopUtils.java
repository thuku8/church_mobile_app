package com.app.sample.shop.util;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.view.MaterialRippleLayout;

/**
 * Created by shobo on 6/6/17.
 */

//we can also add the implements OnClickListener
public class DialogShopUtils{

    private Context ctx;
	private Dialog mDialog;

    private MaterialRippleLayout dialogActionsLayout;

	private TextView mOKButton;
	private TextView mCheck1;
	private TextView mCheck2;

    private int selectedType = 0;

	private DialogShopUtils.OnItemClickListener mOnItemClickListener;

    public void setCancellable(boolean cancellable) {
        mDialog.setCancelable(cancellable);
    }

    public interface OnItemClickListener {
		void onItemClick(View view, int selectedType);
	}

	public void SetOnClickListener(final DialogShopUtils.OnItemClickListener mItemClickListener) {
		this.mOnItemClickListener = mItemClickListener;
	}

	public DialogShopUtils(Context ctx) {
		this.ctx = ctx;
	}

	public void showDialog() {
		if (mDialog == null) {
			mDialog = new Dialog(ctx, R.style.CustomDialogTheme);
		}
		mDialog.setContentView(R.layout.dialog_payment_types);
		mDialog.show();

		mCheck1 = (TextView) mDialog.findViewById(R.id.dialog_shop_check1);
		mCheck2 = (TextView) mDialog.findViewById(R.id.dialog_shop_check2);
		mOKButton = (TextView) mDialog.findViewById(R.id.dialog_shop_ok);
        dialogActionsLayout = (MaterialRippleLayout) mDialog.findViewById(R.id.dialogActionsLayout);
        dialogActionsLayout.setVisibility(View.GONE);
        mOKButton.setClickable(true);
		initDialogButtons();
	}

	private void initDialogButtons() {

        //ewallet pay type
        mCheck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {

                    if (mCheck1.getText() == ctx.getString(R.string.material_icon_check_empty)) {
                        mCheck1.setText(ctx.getString(R.string.material_icon_checked_full));
                        mCheck2.setText(ctx.getString(R.string.material_icon_check_empty));
//                        mCheck2.setClickable(false);
                        dialogActionsLayout.setVisibility(View.VISIBLE);
//                        mOKButton.setClickable(true);
                        selectedType = 1;

                    } else {
                        mCheck1.setText(ctx.getString(R.string.material_icon_check_empty));
//                        mCheck2.setClickable(true);
                        dialogActionsLayout.setVisibility(View.GONE);
//                        mOKButton.setClickable(false);
                        selectedType = 0;
                    }

                    mOnItemClickListener.onItemClick(view, selectedType);
                }
            }
        });

        //lipa na mpesa pay type
        mCheck2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {

                    if (mCheck2.getText() == ctx.getString(R.string.material_icon_check_empty)) {
                        mCheck2.setText(ctx.getString(R.string.material_icon_checked_full));
                        mCheck1.setText(ctx.getString(R.string.material_icon_check_empty));
//                        mCheck1.setClickable(false);
                        dialogActionsLayout.setVisibility(View.VISIBLE);
//                        mOKButton.setClickable(true);
                        selectedType = 2;
                    } else {
                        mCheck2.setText(ctx.getString(R.string.material_icon_check_empty));
//                        mCheck1.setClickable(true);
                        dialogActionsLayout.setVisibility(View.GONE);
//                        mOKButton.setClickable(false);
                        selectedType = 0;

                    }
                    mOnItemClickListener.onItemClick(view, selectedType);
                }
            }
        });


		mOKButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mOnItemClickListener != null) {
					mOnItemClickListener.onItemClick(view,selectedType);

                    //reset all the checked inputs and string
                    mCheck2.setText(ctx.getString(R.string.material_icon_check_empty));
                    mCheck1.setText(ctx.getString(R.string.material_icon_check_empty));
//                        mCheck1.setClickable(false);
                    dialogActionsLayout.setVisibility(View.GONE);
//                        mOKButton.setClickable(false);
                    selectedType = 0;
				}
			}
		});
	}

	public void dismissDialog() {
		mDialog.dismiss();
	}

}
