package com.app.sample.shop.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.font.RobotoTextView;

/**
 * Created by shobo on 5/11/17.
 */

public class SplashPagerFragment extends Fragment {

    private static final String ARG_POSITION = "position";

    private int position;
    private RelativeLayout layout;
    private TextView icon;
    private RobotoTextView text;
    private ImageView frag_img;



    public static SplashPagerFragment newInstance(int position) {
        SplashPagerFragment f = new SplashPagerFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_wizard_splash_pager,
                container, false);

        layout = (RelativeLayout) rootView
                .findViewById(R.id.fragment_layout);

        icon = (TextView) rootView
                .findViewById(R.id.fragment_icon);

        text = (RobotoTextView) rootView
                .findViewById(R.id.fragment_text);

        frag_img = (ImageView) rootView
                .findViewById(R.id.fragment_image);

        if (position == 0) {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_300));
            layout.invalidate();
            icon.setText(R.string.material_icon_cart);
            setUpFragmentImage(R.drawable.fragment_portrait_1);
        } else if (position == 1) {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_700));
            layout.invalidate();
            icon.setText(R.string.material_icon_headphones);
            setUpFragmentImage(R.drawable.fragment_portrait_2);

        } else {
            layout.setBackgroundColor(getResources().getColor(
                    R.color.material_purple_900));
            layout.invalidate();
            icon.setText(R.string.material_icon_w_shop);
            setUpFragmentImage(R.drawable.fragment_portrait_3);
        }

        ViewCompat.setElevation(rootView, 50);
        return rootView;
    }

    private void setUpFragmentImage(int drawable) {
        // need to use .setBackground and .setBackgroundDrawable depending on the
        // android version because .setBackgroundDrawable is depreciated
        int sdk = android.os.Build.VERSION.SDK_INT;
        int jellyBean = android.os.Build.VERSION_CODES.JELLY_BEAN;
        if(sdk < jellyBean) {
            frag_img.setBackgroundResource(drawable);
        } else {
            frag_img.setBackgroundResource(drawable);
        }

    }

}