package com.app.sample.shop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.ItemModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shobo on 5/16/17.
 */

public class FeaturedItemsGridAdapter extends RecyclerView.Adapter<FeaturedItemsGridAdapter.ViewHolder> implements Filterable{

    private final int mBackground;
    private List<ItemModel> original_items = new ArrayList<>();
    private List<ItemModel> filtered_items = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();
    private final TypedValue mTypedValue = new TypedValue();

    private Context ctx;

    // for item click listener
    private OnItemClickListener mOnItemClickListener;
    public interface OnItemClickListener {
        void onItemClick(View view, ItemModel obj, int position);
    }
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public TextView dealerName;
//        public TextView like;
        public TextView price;
        public TextView priceOrig;
        public TextView priceCutPercent;
        private TextView addToCartNow;
        //private MaterialDesignIconsTextView addToCartNow;
        private TextView buyNowTxt;
        public ImageView image;
        public RelativeLayout lyt_parent;


        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            dealerName = (TextView) v.findViewById(R.id.dealerName);
//            like = (TextView) v.findViewById(R.id.like);
            price = (TextView) v.findViewById(R.id.price);
            priceCutPercent = (TextView) v.findViewById(R.id.priceCutPercent);
            priceOrig = (TextView) v.findViewById(R.id.priceOrig);
            image = (ImageView) v.findViewById(R.id.image);
            buyNowTxt = (TextView) v.findViewById(R.id.buyNowTxt);
            addToCartNow = (TextView) v.findViewById(R.id.addToCartNow);
            lyt_parent = (RelativeLayout) v.findViewById(R.id.lyt_parent);
        }

    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FeaturedItemsGridAdapter(Context ctx, List<ItemModel> items) {
        this.ctx = ctx;
        original_items = items;
        filtered_items = items;
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
    }

    @Override
    public FeaturedItemsGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.featured_item_grid, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ItemModel p = filtered_items.get(position);
        holder.title.setText(p.getName());
        holder.dealerName.setText(p.getDealerName());
//        holder.like.setText(p.getLikes());
        holder.price.setText(p.getStrPrice());
        holder.priceOrig.setText(p.getStrOrigPrice());
        holder.priceCutPercent.setText(p.getStrCutPricePercent());
        Picasso.with(ctx).load(p.getImg()).into(holder.image);
//
//        // Here you apply the animation when the view is bound
        setAnimation(holder.itemView, position);
//
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(view, p, position);
                }
            }
        });

        holder.buyNowTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(view, p, position);
                }
            }
        });

        holder.addToCartNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(view, p, position);
                }
            }
        });
    }

    /**
     * Here is the key method to apply the animation
     */
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx, R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();
            final List<ItemModel> list = original_items;
            final List<ItemModel> result_list = new ArrayList<>(list.size());

            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getName();
                String str_cat = list.get(i).getCategory();
                if (str_title.toLowerCase().contains(query) || str_cat.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }

            results.values = result_list;
            results.count = result_list.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<ItemModel>) results.values;
            notifyDataSetChanged();
        }
    }

}
