package com.app.sample.shop.activities.Orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.app.sample.shop.ActivityMain;
import com.app.sample.shop.R;
import com.app.sample.shop.adapter.OrderProductsAdapter;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.model.Orders;

/**
 * Created by shobo on 6/8/17.
 */

//public class OrderDetailsActivity extends AppCompatActivity {

public class OrderDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_OBJCT = "com.app.sample.shop.ITEM";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Orders obj) {
        Intent intent = new Intent(activity, OrderDetailsActivity.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Orders orderModel;
    private ActionBar actionBar;
    private GlobalVariable global;
    private View parent_view;

    //recommended products
    private RecyclerView prodsRecyclerView;
    private OrderProductsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        parent_view = findViewById(android.R.id.content);

        // animation transition
        ViewCompat.setTransitionName(findViewById(R.id.image), EXTRA_OBJCT);

        global = (GlobalVariable) getApplication();

        // get extra object
        orderModel = (Orders) getIntent().getSerializableExtra(EXTRA_OBJCT);


        initToolbar();

        ((TextView) findViewById(R.id.ordernumber)).setText("Order#:"+orderModel.getOrderId());
        ((TextView) findViewById(R.id.totalOrderCost)).setText(orderModel.getOrderTotal());
        ((TextView) findViewById(R.id.dateOrdered)).setText("Delivery date: "+orderModel.getDate());
        ((TextView) findViewById(R.id.deliveryPlace)).setText(orderModel.getDeliveryName());
        ((TextView) findViewById(R.id.deliveryDistance)).setText(orderModel.getDistance()+ "Kms");
        ((TextView) findViewById(R.id.paymentTypeTxt)).setText(orderModel.getPaymentType());

        prodsRecyclerView = (RecyclerView) findViewById(R.id.prodsRecyclerView);
        mAdapter = new OrderProductsAdapter(this,orderModel.getOrderItems());
        prodsRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        prodsRecyclerView.setLayoutManager(mLayoutManager);
        prodsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        prodsRecyclerView.setAdapter(mAdapter);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                break;
//        }
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent i = new Intent(OrderDetailsActivity.this, ActivityMain.class);
                Bundle b = new Bundle();
                b.putString("fragments", "launch_orders");
                i.putExtra("launch_fragments", b);
                startActivity(i);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }


    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Order Summary");
    }


}