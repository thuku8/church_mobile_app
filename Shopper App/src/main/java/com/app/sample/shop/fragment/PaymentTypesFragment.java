package com.app.sample.shop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.sample.shop.R;
import com.app.sample.shop.activities.Payments.ActivityCheckOut;
import com.app.sample.shop.adapter.CartListAdapter;
import com.app.sample.shop.data.GlobalVariable;
import com.app.sample.shop.model.ItemModel;
import com.app.sample.shop.model.OrderProduct;
import com.app.sample.shop.model.Orders;
import com.app.sample.shop.util.DialogShopUtils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shobo on 6/7/17.
 */

public class PaymentTypesFragment extends Fragment {


    private View view;
    private GlobalVariable global;

    private String payment_type = "";
    private String payment_type_id = "";
    private DialogShopUtils dialog;

    private String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private SecureRandom rnd;

    public PaymentTypesFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_checkout_dialog,
                container, false);
        global = (GlobalVariable) getActivity().getApplication();

        rnd = new SecureRandom();

        ((ActivityCheckOut) getActivity()).setActionBarTitle(ActivityCheckOut.PAYMENT_TYPES_STRING);

        Log.e("ROUTERS", String.valueOf(getArguments().getString(ActivityCheckOut.TAG_PLACE_NAME)));

        dialog = new DialogShopUtils(getActivity());
        dialog.showDialog();
        dialog.setCancellable(false);

        dialog.SetOnClickListener(new DialogShopUtils.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int selectedType) {

                switch (view.getId()){
                    case R.id.dialog_shop_ok:

                        if(selectedType == 1){
                            payment_type = getString(R.string.ewalley_pay_type);
                            payment_type_id = String.valueOf(selectedType);
                        }else{
                            payment_type = getString(R.string.mpesa_pay_type);
                            payment_type_id = String.valueOf(selectedType);
                        }
                        toNextFragment(payment_type,payment_type_id);
                        break;
                    default:
                        break;
                }

            }
        });

        return view;
    }

    private void toNextFragment(String payment_type, String payment_type_id) {

        List<ItemModel> items = new ArrayList<>();
        items = global.getCart();

        //creating an order object//creating an order object//creating an order object//creating an order object
        Date dateNow = new Date();
        String orderDate = String.valueOf(dateNow.getDate()) +"/"+String.valueOf(dateNow.getMonth()+1)+"/"+String.valueOf(dateNow.getYear());
        String deliveryDate = String.valueOf(dateNow.getDate()+3) +"/"+String.valueOf(dateNow.getMonth()+1)+"/"+String.valueOf(dateNow.getYear());

        Orders order = new Orders();
        order.setOrderId(randomString(7));
        order.setDate(String.valueOf(deliveryDate));
        order.setOrderTotal(String.valueOf(global.getCartPriceTotal()));
        order.setOrderItemsTotal(String.valueOf(global.getCartItemTotal()));
        order.setDeliveryName(String.valueOf(getArguments().getString(ActivityCheckOut.TAG_PLACE_NAME)));
        order.setDistance(String.valueOf(getArguments().getString(ActivityCheckOut.TAG_PLACE_DISTANCE)));
        order.setPaymentType(payment_type);

        List <OrderProduct> opList = new ArrayList<>();
        for (int j = 0;j < items.size();j++) {
            //add product ids to array
            OrderProduct op = new OrderProduct(global.getCart().get(j).getId(),global.getCart().get(j).getPrice(),global.getCart().get(j).getTotal());
            op.setName(global.getCart().get(j).getName());
            op.setImg(global.getCart().get(j).getImg());
            opList.add(op);
            order.setOrderItems(opList);
        }
        //then add the order to the orders list
        global.addOrders(order);

        //creating an order object//creating an order object//creating an order object//creating an order object

        Fragment fragment = null;
        Bundle bundle = new Bundle();
        fragment = new PaymentsFinalFragment();
        bundle.putSerializable(ActivityCheckOut.TAG_ORDER_MODEL, order);
        fragment.setArguments(bundle);


        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment,ActivityCheckOut.PAYMENT_FINAL_STRING);
            fragmentTransaction.commit();
        }
        dialog.dismissDialog();

        //clear the adapter
        CartListAdapter mAdapter;
        global.clearCart();
        mAdapter = new CartListAdapter(getActivity(), global.getCart());
        mAdapter.notifyDataSetChanged();
        Snackbar.make(view, "Checkout success", Snackbar.LENGTH_SHORT).show();


    }

    String randomString( int len ){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }
}
