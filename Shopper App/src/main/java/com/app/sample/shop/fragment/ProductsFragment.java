package com.app.sample.shop.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.sample.shop.ActivityMain;
import com.app.sample.shop.ItemDetailsActivity;
import com.app.sample.shop.R;
import com.app.sample.shop.adapter.ProductsItemGridAdapter;
import com.app.sample.shop.data.Constant;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.model.ItemModel;
import com.app.sample.shop.view.range.RangeBar;

import java.util.ArrayList;
import java.util.List;
//import android.view.View.OnClickListener;

/**
 * Created by shobo on 5/29/17.
 */

public class ProductsFragment extends Fragment implements View.OnClickListener {

    public static String TAG_CATEGORY = "com.app.sample.shop.tagCategory";
    public static String TAG_SORT = "com.app.sample.shop.tagSort";

    private View view;
    private RecyclerView recyclerView;
    private ProductsItemGridAdapter mAdapter;
    private LinearLayout lyt_notfound;
    private String category = "";
    private String sort = "";

    private TextView mFilters;
    private TextView filterTxtViewIconArrow;
    private LinearLayout mFiltersLayout;

    //for the filter layout
    private RangeBar rangebar;
    Typeface fonts1;
    LinearLayout linear1,linear2, linear3, linear4, linear5, linear6, linear7, linear8;
    TextView ss, m, l, xl, xxl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category, container,false);
        category = getArguments().getString(TAG_CATEGORY);
        sort = getArguments().getString(TAG_SORT);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        lyt_notfound = (LinearLayout) view.findViewById(R.id.lyt_notfound);

        //the filters layout//the filters layout//the filters layout//the filters layout//the filters layout
        mFilters = (TextView) view.findViewById(R.id.filterTxtView);
        filterTxtViewIconArrow = (TextView) view.findViewById(R.id.filterTxtViewIconArrow);
        mFiltersLayout = (LinearLayout) view.findViewById(R.id.search_bar_filters_layout);

        fonts1 =  Typeface.createFromAsset(getActivity().getAssets(), "fonts/MavenPro-Regular.ttf");

        Button t5 =(Button) view.findViewById(R.id.rang1);
        t5.setTypeface(fonts1);
        Button t6 =(Button) view.findViewById(R.id.rang2);
        t6.setTypeface(fonts1);
        ss = (TextView)  view.findViewById(R.id.ss);
        m = (TextView)  view.findViewById(R.id.m);
        l = (TextView)  view.findViewById(R.id.l);
        xl = (TextView)  view.findViewById(R.id.xl);
        xxl = (TextView)  view.findViewById(R.id.xxl);



        ss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clickb("S", "1");

            }
        });


        m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickb("M", "2");

            }
        });
        l.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickb("L", "3");

            }
        });
        xl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickb("XL", "4");

            }
        });
        xxl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                clickb("XXL", "5");

            }
        });




        rangebar = (RangeBar) view.findViewById(R.id.rangebar1);



        final Button leftIndexValue = (Button) view.findViewById(R.id.rang1);
        final Button rightIndexValue = (Button) view.findViewById(R.id.rang2);

        // Sets the display values of the indices
        rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex,
                                              int rightPinIndex,
                                              String leftPinValue, String rightPinValue) {
                leftIndexValue.setText("$" + leftPinIndex);
                rightIndexValue.setText("$" + rightPinIndex);
            }

        });






//        ***********kids**********

        linear1 = (LinearLayout)view.findViewById(R.id.linear1);
        linear2 = (LinearLayout)view.findViewById(R.id.linear2);


        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear2.setVisibility(View.VISIBLE);
                linear1.setVisibility(View.GONE);

            }
        });

        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear2.setVisibility(View.GONE);
                linear1.setVisibility(View.VISIBLE);



            }
        });

//        ***********man**********

        linear3 = (LinearLayout)view.findViewById(R.id.linear3);
        linear4 = (LinearLayout)view.findViewById(R.id.linear4);


        linear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear4.setVisibility(View.VISIBLE);
                linear3.setVisibility(View.GONE);

            }
        });

        linear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear4.setVisibility(View.GONE);
                linear3.setVisibility(View.VISIBLE);



            }
        });

//        ***********women**********

        linear5 = (LinearLayout)view.findViewById(R.id.linear5);
        linear6 = (LinearLayout)view.findViewById(R.id.linear6);


        linear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear6.setVisibility(View.VISIBLE);
                linear5.setVisibility(View.GONE);

            }
        });

        linear6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear6.setVisibility(View.GONE);
                linear5.setVisibility(View.VISIBLE);



            }
        });

//        ***********all**********

        linear7 = (LinearLayout)view.findViewById(R.id.linear7);
        linear8 = (LinearLayout)view.findViewById(R.id.linear8);


        linear7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear8.setVisibility(View.VISIBLE);
                linear7.setVisibility(View.GONE);

            }
        });

        linear8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                linear8.setVisibility(View.GONE);
                linear7.setVisibility(View.VISIBLE);



            }
        });

        //the filters layout//the filters layout//the filters layout//the filters layout

        mFilters.setOnClickListener(this);
        filterTxtViewIconArrow.setOnClickListener(this);

        LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), Tools.getGridSpanCount(getActivity()));

        //StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(3,1);
        recyclerView.setLayoutManager(mLayoutManager);

        //set data and list adapter
        List<ItemModel> items = new ArrayList<>();
        if(category.equals(getString(R.string.menu_clothing))){

            if(sort.equals("Female")){
                items = Constant.getItemClothes(getActivity(),"Female");
            }else if(sort.equals("Male")){
                items = Constant.getItemClothes(getActivity(),"Male");
            }else{//you can also check if the string is equals to 'All'
                items = Constant.getItemClothes(getActivity(),"");
            }
        }else if(category.equals(getString(R.string.menu_shoes))){
            if(sort.equals("Female")){
                items = Constant.getItemShoes(getActivity(),"Female");
            }else if(sort.equals("Male")){
                items = Constant.getItemShoes(getActivity(),"Male");
            }else{//you can also check if the string is equals to 'All'
                items = Constant.getItemShoes(getActivity(),"");
            }
        }else if(category.equals(getString(R.string.menu_watches))){
            if(sort.equals("Female")){
                items = Constant.getItemWatches(getActivity(),"Female");
            }else if(sort.equals("Male")){
                items = Constant.getItemWatches(getActivity(),"Male");
            }else{//you can also check if the string is equals to 'All'
                items = Constant.getItemWatches(getActivity(),"");
            }
        }else if(category.equals(getString(R.string.menu_accessories))){
            if(sort.equals("Female")){
                items = Constant.getItemAccessories(getActivity(),"Female");
            }else if(sort.equals("Male")){
                items = Constant.getItemAccessories(getActivity(),"Male");
            }else{//you can also check if the string is equals to 'All'
                items = Constant.getItemAccessories(getActivity(),"");
            }
        }else if(category.equals(getString(R.string.menu_bags))){
            if(sort.equals("Female")){
                items = Constant.getItemBags(getActivity(),"Female");
            }else if(sort.equals("Male")){
                items = Constant.getItemBags(getActivity(),"Male");
            }else{//you can also check if the string is equals to 'All'
                items = Constant.getItemBags(getActivity(),"");
            }
        }else if(category.equals(getString(R.string.menu_new))){
            items = Constant.getItemNew(getActivity());
        }
        mAdapter = new ProductsItemGridAdapter(getActivity(), items);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new ProductsItemGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, ItemModel obj, int position) {
                ItemDetailsActivity.navigate((ActivityMain)getActivity(), v.findViewById(R.id.lyt_parent), obj);
            }
        });

        if(mAdapter.getItemCount()==0){
            lyt_notfound.setVisibility(View.VISIBLE);
        }else{
            lyt_notfound.setVisibility(View.GONE);
        }
        return view;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filterTxtView:
                if(mFiltersLayout.getVisibility() == View.VISIBLE) {
                    mFiltersLayout.setVisibility(View.GONE);
                } else {
                    mFiltersLayout.setVisibility(View.VISIBLE);
                }
                if (filterTxtViewIconArrow.getText() == getString(R.string.material_icon_chevron_up)) {
                    filterTxtViewIconArrow.setText(getString(R.string.material_icon_chevron_down));
                } else {
                    filterTxtViewIconArrow.setText(getString(R.string.material_icon_chevron_up));
                }
                break;

            case R.id.filterTxtViewIconArrow:
                if(mFiltersLayout.getVisibility() == View.VISIBLE) {
                    mFiltersLayout.setVisibility(View.GONE);
                } else {
                    mFiltersLayout.setVisibility(View.VISIBLE);
                }
                if (filterTxtViewIconArrow.getText() == getString(R.string.material_icon_chevron_up)) {
                    filterTxtViewIconArrow.setText(getString(R.string.material_icon_chevron_down));
                } else {
                    filterTxtViewIconArrow.setText(getString(R.string.material_icon_chevron_up));
                }
                break;
        }
    }

    private void clickb(String s , String t) {

        ss.setBackgroundResource(R.drawable.sizeround1);
        ss.setTextColor(Color.parseColor("#585858"));
        m.setBackgroundResource(R.drawable.sizeround1);
        m.setTextColor(Color.parseColor("#585858"));
        l.setBackgroundResource(R.drawable.sizeround1);
        l.setTextColor(Color.parseColor("#585858"));
        xl.setBackgroundResource(R.drawable.sizeround1);
        xl.setTextColor(Color.parseColor("#585858"));
        xxl.setBackgroundResource(R.drawable.sizeround1);
        xxl.setTextColor(Color.parseColor("#585858"));

        if(t.equalsIgnoreCase("1")) {
            ss.setBackgroundResource(R.drawable.sizeround2);
            ss.setTextColor(Color.parseColor("#ffffff"));
        }
        else if(t.equalsIgnoreCase("2")){
            m.setBackgroundResource(R.drawable.sizeround2);
            m.setTextColor(Color.parseColor("#ffffff"));
        }

        else if(t.equalsIgnoreCase("3")){
            l.setBackgroundResource(R.drawable.sizeround2);
            l.setTextColor(Color.parseColor("#ffffff"));
        }
        else if(t.equalsIgnoreCase("4")){
            xl.setBackgroundResource(R.drawable.sizeround2);
            xl.setTextColor(Color.parseColor("#ffffff"));
        }
        else if(t.equalsIgnoreCase("5")){
            xxl.setBackgroundResource(R.drawable.sizeround2);
            xxl.setTextColor(Color.parseColor("#ffffff"));
        }

    }


}
