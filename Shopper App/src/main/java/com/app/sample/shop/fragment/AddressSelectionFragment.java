package com.app.sample.shop.fragment;

import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.sample.shop.R;
import com.app.sample.shop.activities.Payments.ActivityCheckOut;
import com.app.sample.shop.data.GlobalVariable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DecimalFormat;

/**
 * Created by shobo on 6/8/17.
 */

public class AddressSelectionFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{
    private View view;
    private GlobalVariable global;


    private static final String TAG = "AddressSelection";

    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final float DEFAULT_ZOOM = 12.0f;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    private CameraPosition mCameraPosition;
    private static LatLng mDefaultLocation = new LatLng(-1.2630112, 36.8417109);
    private static Location mLastKnownLocation;
    private static LatLng mSelectedLocation;

    private boolean mLocationPermissionGranted;

    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";

    private Button proceed;
    private SupportMapFragment mapFragment;
    private Bundle bundle;
    private String placeName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.activity_address_selection, null);
        view = inflater.inflate(R.layout.activity_address_selection, null, false);
        global = (GlobalVariable) getActivity().getApplication();

        ((ActivityCheckOut) getActivity()).setActionBarTitle(ActivityCheckOut.PAYMENT_ADDRESS_STRING);

        initAutoComplete();

        Button showModalBottomSheet = (Button) view.findViewById(R.id.as_modal);
        showModalBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Initializing
                BottomSheetDialogFragment bottomSheetDialogFragment = new SavedLocationsFragment();

                //showing
                bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();

        proceed = (Button) view.findViewById(R.id.proceed);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proceed(v);
            }
        });

        return view;
    }

    private void initAutoComplete() {
        PlaceAutocompleteFragment autocompleteFragment;
        autocompleteFragment = (PlaceAutocompleteFragment) getActivity().
                getFragmentManager().findFragmentById(R.id.selected_address);

        autocompleteFragment.setHint("Search Location");
        //((EditText)autocompleteFragment.getView().findViewById(R.id.selected_address)).setTextSize(10.0f);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                placeName = String.valueOf(place.getName());
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(place.getLatLng()).title((String) place.getName()).draggable(true));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));

                mSelectedLocation = place.getLatLng();
                proceed.setVisibility(View.VISIBLE);

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(latLng).title(latLng.toString()).draggable(true));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.0f));

                mSelectedLocation = latLng;
                proceed.setVisibility(View.VISIBLE);
            }
        });

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

    }

    public void proceed(View view) {

        /* For Calculating Distance */
        LatLng start = new LatLng(mSelectedLocation.latitude, mSelectedLocation.longitude);
        LatLng end = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);
        callNextFragment(mSelectedLocation.latitude,mSelectedLocation.longitude,getDistanceBetweenLocations(start, end));


    }

    private void callNextFragment(double latitude, double longitude, double distanceBetweenLocations) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        fragment = new PaymentTypesFragment();
        bundle.putString(ActivityCheckOut.TAG_LONG, String.valueOf(latitude));
        bundle.putString(ActivityCheckOut.TAG_LAT, String.valueOf(longitude));
        bundle.putString(ActivityCheckOut.TAG_PLACE_DISTANCE,String.valueOf(distanceBetweenLocations));
        if(placeName == null){
            placeName = "Chosen Address";
        }
        bundle.putString(ActivityCheckOut.TAG_PLACE_NAME, String.valueOf(placeName));
        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment);
            fragmentTransaction.commit();
        }
    }

    public static void useCurrentLocation(View view) {
        /* For Calculating Distance */
        LatLng start = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
        LatLng end = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);
    }


    public static double getDistanceBetweenLocations(LatLng startLatLng, LatLng endLatLng) {

        int Radius = 6371;// radius of earth in Km

        double startLat = startLatLng.latitude;
        double startLng = startLatLng.longitude;

        double endLat = endLatLng.latitude;
        double endLng = endLatLng.longitude;

        double dLat = Math.toRadians(endLat - startLat);
        double dLon = Math.toRadians(endLng - startLng);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(startLat))
                * Math.cos(Math.toRadians(endLat)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);

        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;

        DecimalFormat newFormat = new DecimalFormat("####");

        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));

//        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
//                + " Meter   " + meterInDec);

        return Math.round(Radius * c);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    private void getDeviceLocation() {
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mLocationPermissionGranted) {
            mLastKnownLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        }

        // Set the map's camera position to the current location of the device.
        if (mCameraPosition != null) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
        } else if (mLastKnownLocation != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(mLastKnownLocation.getLatitude(),
                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
        } else {
            Log.e(TAG, "Current location is null. Using defaults.");
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mLocationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mLastKnownLocation = null;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public static class SavedLocationsFragment extends BottomSheetDialogFragment {

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
        }

        private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        };

        @Override
        public void setupDialog(final Dialog dialog, int style) {
            super.setupDialog(dialog, style);

            View contentView = View.inflate(getContext(), R.layout.item_saved_locations, null);
            dialog.setContentView(contentView);
            CoordinatorLayout.LayoutParams layoutParams =
                    (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
            CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
            if (behavior != null && behavior instanceof BottomSheetBehavior) {
                ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
            }

            Button btn1 = (Button) contentView.findViewById(R.id.use_current);
            btn1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    useCurrentLocation1(v);
                    dialog.dismiss();
                }

            });
        }

        private void useCurrentLocation1(View v) {
            /* For Calculating Distance */
            LatLng start = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            LatLng end = new LatLng(mDefaultLocation.latitude, mDefaultLocation.longitude);


            callNextFragment(mLastKnownLocation.getLatitude(),mLastKnownLocation.getLongitude(),getDistanceBetweenLocations(start, end));
        }

        private void callNextFragment(double latitude, double longitude, double distanceBetweenLocations) {

            Fragment fragment = null;
            Bundle bundle = new Bundle();

            fragment = new PaymentTypesFragment();
            bundle.putString(ActivityCheckOut.TAG_LONG, String.valueOf(latitude));
            bundle.putString(ActivityCheckOut.TAG_LAT, String.valueOf(longitude));
            bundle.putString(ActivityCheckOut.TAG_PLACE_DISTANCE,String.valueOf(distanceBetweenLocations));
            bundle.putString(ActivityCheckOut.TAG_PLACE_NAME, String.valueOf("Chosen Address"));
            fragment.setArguments(bundle);

            if (fragment != null) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_content, fragment);
                fragmentTransaction.commit();
            }
        }
    }


}
