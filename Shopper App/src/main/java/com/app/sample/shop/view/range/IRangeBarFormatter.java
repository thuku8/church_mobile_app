package com.app.sample.shop.view.range;

/**
 * Created by franciscomorais on 24/06/15.
 */
public interface IRangeBarFormatter {

    String format(String value);

}
