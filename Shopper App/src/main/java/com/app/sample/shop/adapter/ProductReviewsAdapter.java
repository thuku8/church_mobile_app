package com.app.sample.shop.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.Reviews;

import java.util.ArrayList;


public class ProductReviewsAdapter extends BaseAdapter {

    Context context;

    ArrayList<Reviews> review;
    Typeface fonts1,fonts2;

    public ProductReviewsAdapter(Context context, ArrayList<Reviews> review) {
        this.context = context;
        this.review = review;
    }

    @Override
    public int getCount() {
        return review.size();
    }

    @Override
    public Object getItem(int position) {
        return review.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        fonts1 =  Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Light.ttf");
        fonts2 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Regular.ttf");

        ViewHolder viewHolder = null;

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.reviews_list,null);
            viewHolder = new ViewHolder();
            viewHolder.image = (ImageView)convertView.findViewById(R.id.image);
            viewHolder.title = (TextView)convertView.findViewById(R.id.title);
            viewHolder.discription = (TextView)convertView.findViewById(R.id.description);
            viewHolder.date = (TextView)convertView.findViewById(R.id.date);
            viewHolder.title.setTypeface(fonts2);
            viewHolder.discription.setTypeface(fonts1);
            viewHolder.date.setTypeface(fonts2);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Reviews review = (Reviews) getItem(position);
        viewHolder.image.setImageResource(review.getImage());
        viewHolder.title.setText(review.getTitle());
        viewHolder.discription.setText(review.getDiscription());
        viewHolder.date.setText(review.getDate());
        return convertView;
    }

    private class ViewHolder{
        ImageView image;
        TextView title;
        TextView discription;
        TextView date;





    }
}




