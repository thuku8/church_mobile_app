package com.app.sample.shop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.model.Orders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shobo on 6/7/17.
 */

public class OrdersListAdapter extends RecyclerView.Adapter<OrdersListAdapter.ViewHolder> implements Filterable {

    private final int mBackground;
    private List<Orders> original_items = new ArrayList<>();
    private List<Orders> filtered_items = new ArrayList<>();
    private OrdersListAdapter.ItemFilter mFilter = new OrdersListAdapter.ItemFilter();

    private Orders p;

    private final TypedValue mTypedValue = new TypedValue();

    private Context ctx;
    private OrdersListAdapter.OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Orders obj);
    }

    public void SetOnItemClickListener(final OrdersListAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView orderNo;
        public TextView orderDate;
        public TextView orderTotal;

        public LinearLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            orderNo = (TextView) v.findViewById(R.id.orderNo);
            orderDate = (TextView) v.findViewById(R.id.orderDate);
            orderTotal = (TextView) v.findViewById(R.id.orderTotal);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public OrdersListAdapter(Context ctx, List<Orders> items) {
        this.ctx = ctx;
        original_items = items;
        filtered_items = items;
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
    }

    @Override
    public OrdersListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orders, parent, false);
        //v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        OrdersListAdapter.ViewHolder vh = new OrdersListAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final OrdersListAdapter.ViewHolder holder, final int position) {
        p = filtered_items.get(position);
        holder.orderNo.setText("#"+p.getOrderId());
        holder.orderDate.setText("Order Date: "+p.getDate());
        holder.orderTotal.setText(p.getOrderTotal()+"$");

        //on click
        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, p);
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();
            final List<Orders> list = original_items;
            final List<Orders> result_list = new ArrayList<>(list.size());

            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getOrderId();
                //this should be sorted later
                String str_cat = list.get(i).getDate();
                if (str_title.toLowerCase().contains(query) || str_cat.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }

            results.values = result_list;
            results.count = result_list.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<Orders>) results.values;
            notifyDataSetChanged();
        }

    }
}
