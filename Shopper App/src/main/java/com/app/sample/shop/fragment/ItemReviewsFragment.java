package com.app.sample.shop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.sample.shop.R;
import com.app.sample.shop.adapter.ProductReviewsAdapter;
import com.app.sample.shop.data.ExpandableHeightListView;
import com.app.sample.shop.model.Reviews;

import java.util.ArrayList;

/**
 * Created by shobo on 5/18/17.
 */

public class ItemReviewsFragment extends Fragment {


    private ExpandableHeightListView ratingsListview;
    private ArrayList<Reviews> reviews;
    private ProductReviewsAdapter baseAdapter;


    private int[] IMAGE = {R.drawable.c_m_1, R.drawable.ic_friend_dark, R.drawable.c_f_1, R.drawable.c_m_6, R.drawable.s_m_7};
    private String[] TITLE = {"Best seller", "Dunt think twise for it", "Good product", "Dunt think twise for it", "Good product"};
    private String[] RATING = {"4.5 rating", "5 rating", "4 rating", "5 rating", "4 rating"};
    private String[] BY = {"by Kelly","by Emma","by Erik","by Emma","by Erik"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_reviews, null);
//        LinearLayout lyt_content = (LinearLayout) view.findViewById(R.id.lyt_content);
//        //lyt_content.setVisibility(View.GONE);
//        //TypedArray feed_photo = getResources().obtainTypedArray(R.array.feed_photos);
//        // define feed wrapper
//        FeedItemViewWrapper feedItemViewWrapper = new FeedItemViewWrapper(getActivity(), lyt_content);
//        //Friend friend = ActivityFriendDetails.friend;private long id;
//
//        Friend friend = new Friend(12,"Test User",1);
//        feedItemViewWrapper.addItemFeed(new Feed(0, "14:56", friend, getString(R.string.middle_lorem_ipsum)));
//        feedItemViewWrapper.addItemFeed(new Feed(1, "11:30", friend, getString(R.string.middle_lorem_ipsum)));
////        feedItemViewWrapper.addItemFeed(new Feed(2, "09:10", friend, getString(R.string.lorem_ipsum)));
////        feedItemViewWrapper.addItemFeed(new Feed(3, "Yesterday", friend, getString(R.string.short_lorem_ipsum)));
////        feedItemViewWrapper.addItemFeed(new Feed(4, "05 Nov 2015", friend, getString(R.string.long_lorem_ipsum)));


        //        ********RATINGS LISTVIEW***********


        ratingsListview = (ExpandableHeightListView) view.findViewById(R.id.ratingsListview);
        reviews = new ArrayList<Reviews>();

        for (int i= 0; i< TITLE.length; i++){
            Reviews review = new Reviews(IMAGE[i], TITLE[i], RATING[i], BY[i]);
            reviews.add(review);
        }

        baseAdapter = new ProductReviewsAdapter(getActivity(), reviews) {
        };

        ratingsListview.setAdapter(baseAdapter);
        return view;
    }
}
